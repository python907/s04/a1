from abc import ABC, abstractclassmethod

class Animal(ABC):
	@abstractclassmethod
	def eat(self, food):
		pass

	def make_sound(self):
		pass

class Dog(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self.age = age

	# getter of Dog class
	def get_name(self):
		return self._name

	def get_breed(self):
		return self._breed

	def get_nage(self):
		return self._age


	#setters of Dog class
	def set_name(self):
		self._name = name

	def set_breed(self):
		self._breed = breed

	def set_age(self):
		self._age = age

	#implementing the abstrat methodd from the parent class
	def eat(self, food):
		print(f"Eaten the {food}")

	def make_sound(self):
		print(f"Arff")

	def call(self):
		print(f"Here {self._name}")



class Cat(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self.age = age

	# getter of Dog class
	def get_name(self):
		return self._name

	def get_breed(self):
		return self._breed

	def get_nage(self):
		return self._age


	#setters of Dog class
	def set_name(self):
		self._name = name

	def set_breed(self):
		self._breed = breed

	def set_age(self):
		self._age = age

	#implementing the abstrat methodd from the parent class
	def eat(self, food):
		print(f"Eaten the {food}")

	def make_sound(self):
		print(f"Meow")

	def call(self):
		print(f"Here {self._name}")
	

# Test Cases:

dog1 = Dog("Isis", "Dalmination", 15)
dog1.eat("Dog Food")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", "Persian", 11)
cat1.eat("Cat Food")
cat1.make_sound()
cat1.call()






# class Animal():
# 	def __init__(self):
# 		self._name = name
# 		self._breed = breed
# 		self._age = age

# 	# setter methods
# 	def set_name(self, name):
# 		self._name = name 

# 	# getter methods
# 	def get_name(self):
# 		print(f"Name of Animal: {self._name}")

# 	def get_breed(self):
# 		print(f"Age of Animal: {self._breed}")

# 	def get_age(self):
# 		print(f"Age of Animal: {self._age}")


# class Dog (Animal): 
# 	def __init__(self, sound):
# 		super().__init__() 
# 		self._sound = sound

# 	# getter method
# 	def get_sound(self):
# 		print(f" ")

# 	#setter method
# 	def set_sound(self, sound):
# 		self._sound = sound


# 	# details method
# 	def get_details(self):
# 		print(f"{self._sound} belongs to {self._name}")

